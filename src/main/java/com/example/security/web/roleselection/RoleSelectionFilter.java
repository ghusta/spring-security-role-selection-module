package com.example.security.web.roleselection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class RoleSelectionFilter extends GenericFilterBean {

    public static final String SPRING_SECURITY_FORM_ROLE_ID_KEY = "role-id";

    private String roleSelectionFormUrl = "/role-selection";

    private String roleIdParameter = SPRING_SECURITY_FORM_ROLE_ID_KEY;

    private final RequestMatcher roleSelectionMatcher;
    private final RequestMatcher roleSelectionProcessingMatcher;

    private String targetUrl;

    private AuthenticationSuccessHandler successHandler = new SimpleUrlAuthenticationSuccessHandler();

    private final RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    public RoleSelectionFilter() {
        roleSelectionMatcher = new AntPathRequestMatcher(roleSelectionFormUrl, "GET");
        roleSelectionProcessingMatcher = new AntPathRequestMatcher(roleSelectionFormUrl, "POST");
    }

    @Override
    public void afterPropertiesSet() throws ServletException {
        if (this.targetUrl != null) {
            this.successHandler = new SimpleUrlAuthenticationSuccessHandler(this.targetUrl);
        }
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        doFilter((HttpServletRequest) request, (HttpServletResponse) response, chain);
    }

    private void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        logger.debug("Filtering request URI : " + request.getRequestURI());

        if (securityContext == null || securityContext.getAuthentication() == null) {
            logger.debug("No SecurityContext or Authentication");
            chain.doFilter(request, response);
            return;
        }

        if (roleSelectionMatcher.matches(request)) {
            // display form
            logger.debug("Display : " + roleSelectionFormUrl);
            chain.doFilter(request, response);
            return;
        }

        if (roleSelectionProcessingMatcher.matches(request)) {
            logger.debug("processing role selection...");
            String roleId = obtainRoleId(request);
            if (roleId != null) {
                logger.debug("Selected roleId = " + roleId);

                GrantedAuthority selectedGrantedAuthority = findGrantedAuthority(roleId, securityContext.getAuthentication().getAuthorities());
                if (selectedGrantedAuthority != null) {
                    logger.debug("Found matching GrantedAuthority : " + selectedGrantedAuthority.getAuthority());

                    Authentication newAuth = changeAuth(selectedGrantedAuthority, securityContext.getAuthentication());
                    SecurityContextHolder.getContext().setAuthentication(newAuth);
                    // redirect to target url
                    this.successHandler.onAuthenticationSuccess(request, response, newAuth);
                }
            } else {
                logger.warn("no roleId found");
                chain.doFilter(request, response);
            }
            return;
        }

        int nbAuthorities = securityContext.getAuthentication().getAuthorities().size();

        if (nbAuthorities > 1) {
            logger.debug("redirecting to role selection form");
            redirectStrategy.sendRedirect(request, response, roleSelectionFormUrl);
            return;
        } else {
            chain.doFilter(request, response);
            return;
        }
    }

    protected Authentication changeAuth(GrantedAuthority selectedGrantedAuthority, Authentication authentication) {
        if (authentication instanceof UsernamePasswordAuthenticationToken) {
            List<GrantedAuthority> newAuthorities = new ArrayList<>();
            newAuthorities.add(selectedGrantedAuthority);
            UsernamePasswordAuthenticationToken newAuth
                    = new UsernamePasswordAuthenticationToken(authentication.getPrincipal(), authentication.getCredentials(),
                    newAuthorities);
            return newAuth;
        } else {
            logger.warn("Unsupported authentication modification of class : " + authentication.getClass());
            return null;
        }
    }

    private GrantedAuthority findGrantedAuthority(String roleId, Collection<? extends GrantedAuthority> authorities) {
        for (GrantedAuthority authority : authorities) {
            if (authority.hashCode() == Integer.parseInt(roleId)) {
                return authority;
            }
        }
        return null;
    }

    protected String obtainRoleId(HttpServletRequest request) {
        return request.getParameter(this.roleIdParameter);
    }

    public void setTargetUrl(String targetUrl) {
        this.targetUrl = targetUrl;
    }
}
